/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React from 'react';
import { Container } from './app/components';
import AppNaviagation from './app/navigation';

function App() {
  return (
    <Container white>
      <AppNaviagation />
    </Container>
  );
}

export default App;
