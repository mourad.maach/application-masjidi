import {
  AddAdherentPaymentScreen,
  AddAdherentScreen,
  AddMosquePaymentScreen,
  AddMosqueScreen,
  AdherentListScreen,
  DashboardScreen,
  DonationListScreen,
  LoginScreen,
  PasswordForgottenScreen,
  SignupScreen,
  TermsOfServiceScreen,
  WelcomeScreen,
} from '../screens';

import { Image } from 'react-native';
import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { theme } from '../constants';

// import Splash from '../screens/Splash';

const appScreens = createStackNavigator(
  {
    // TODO : add splash screen
    Welcome: { screen: WelcomeScreen },
    Login: { screen: LoginScreen },
    AddAdherent: { screen: AddAdherentScreen },
    AddMosque: { screen: AddMosqueScreen },
    PasswordForgotten: { screen: PasswordForgottenScreen },
    TermsOfService: { screen: TermsOfServiceScreen },
    Dashboard: { screen: DashboardScreen },
    AdherentList: { screen: AdherentListScreen },
    DonationList: { screen: DonationListScreen },
    AddMosquePayment: { screen: AddMosquePaymentScreen },
    AddAdherentPayment: { screen: AddAdherentPaymentScreen },
    Signup: { screen: SignupScreen },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        height: theme.sizes.base * 4,
        backgroundColor: theme.colors.white,
        borderBottomColor: 'transparent',
        elevation: 0,
      },
      headerBackImage: <Image source={require('../assets/icons/back.png')} />,
      headerBackTitle: null,
      headerLeftContainerStyle: {
        alignItems: 'center',
        marginLeft: theme.sizes.base,
        paddingRight: theme.sizes.base,
      },
      headerRightContainerStyle: {
        alignItems: 'center',
        paddingRight: theme.sizes.base,
      },
    },
  },
);

export default createAppContainer(appScreens);
