// @flow

import React, { Component } from 'react';
import { StyleSheet, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

import Text from './Text.component';
import Container from './Container.component';
import Button from './Button.component';
import { theme } from '../constants';

type Props = {
  placeholder: string,
  label: string,
  error: ?any,
  secure: ?boolean,
  rightLabel: ?boolean,
  editable: ?boolean,
  rightStyle: Object,
  onRightPress: () => void,
  email: ?boolean,
  phone: ?boolean,
  number: ?boolean,
  style: Object,
};
type State = { toggleSecure: boolean };

export default class Input extends Component<Props, State> {
  state = {
    toggleSecure: false,
  };

  renderLabel() {
    const { label, error } = this.props;

    return (
      <Container flex={false}>
        {label ? (
          <Text gray2={!error} accent={error}>
            {label}
          </Text>
        ) : null}
      </Container>
    );
  }

  renderToggle() {
    const { secure, rightLabel } = this.props;
    const { toggleSecure } = this.state;

    if (!secure) return null;

    return (
      <Button
        style={styles.toggle}
        onPress={() => this.setState({ toggleSecure: !toggleSecure })}>
        {rightLabel ? (
          rightLabel
        ) : (
          <Icon
            color={theme.colors.gray}
            size={theme.sizes.font * 1.35}
            name={!toggleSecure ? 'eye' : 'eye-with-line'}
          />
        )}
      </Button>
    );
  }

  renderRight() {
    const { rightLabel, rightStyle, onRightPress } = this.props;

    if (!rightLabel) return null;

    return (
      <Button
        style={[styles.toggle, rightStyle]}
        onPress={() => onRightPress && onRightPress()}>
        {rightLabel}
      </Button>
    );
  }

  render() {
    const {
      email,
      phone,
      number,
      secure,
      error,
      style,
      placeholder,
      editable,
      ...props
    } = this.props;

    const { toggleSecure } = this.state;
    const isSecure = toggleSecure ? false : secure;

    const inputStyles = [
      styles.input,
      error && { borderColor: theme.colors.accent },
      style,
    ];

    const inputType = email
      ? 'email-address'
      : number
      ? 'numeric'
      : phone
      ? 'phone-pad'
      : 'default';

    return (
      <Container flex={false} margin={[theme.sizes.base, 0]}>
        {this.renderLabel()}
        <TextInput
          editable={editable}
          style={inputStyles}
          secureTextEntry={isSecure}
          autoComplete="off"
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType={inputType}
          placeholder={placeholder}
          placeholderTextColor={theme.colors.lightGray}
          {...props}
        />
        {this.renderToggle()}
        {this.renderRight()}
      </Container>
    );
  }
  static defaultProps = {
    editable: true,
  };
}

const styles = StyleSheet.create({
  input: {
    backgroundColor: theme.colors.inputBg,
    borderRadius: theme.sizes.radius / 2,
    fontSize: theme.sizes.font,
    fontWeight: '500',
    color: theme.colors.black,
    height: theme.sizes.base * 3,
  },
  toggle: {
    position: 'absolute',
    alignItems: 'flex-end',
    width: theme.sizes.base * 2,
    height: theme.sizes.base * 2,
    top: theme.sizes.base / 5,
    right: 0,
    padding: 0,
    backgroundColor: 'transparent',
  },
});
