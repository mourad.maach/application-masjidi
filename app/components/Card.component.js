// @flow

import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

import Container from './Container.component';
import { theme } from '../constants';

type Props = { color: string, style: any, children: any };
export default class Card extends Component<Props> {
  render() {
    const { color, style, children, ...props } = this.props;
    const cardStyles = [styles.card, style];

    return (
      <Container
        color={color || theme.colors.white}
        style={cardStyles}
        {...props}>
        {children}
      </Container>
    );
  }
}

export const styles = StyleSheet.create({
  card: {
    borderRadius: theme.sizes.radius,
    padding: theme.sizes.base + 4,
    marginBottom: theme.sizes.base,
  },
});
