// @flow

import React from 'react';
import { StyleSheet, ActivityIndicator } from 'react-native';

import Container from './Container.component';

type Props = {
  size: string,
  color: string,
};

function LoadingIndicator(props: Props) {
  const { size, color } = props;

  return (
    <Container block center style={[styles.root]}>
      <ActivityIndicator size={size} color={color} />
    </Container>
  );
}

LoadingIndicator.defaultProps = {
  size: 'large',
  color: '#06BEBD',
};

export default LoadingIndicator;

const styles = StyleSheet.create({
  root: {},
});
