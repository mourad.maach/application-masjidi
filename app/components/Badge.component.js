// @flow

import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

import Container from './Container.component';
import { theme } from '../constants';

export default class Badge extends Component<any> {
  render() {
    const { children, style, size, color, ...props } = this.props;

    const badgeStyles = StyleSheet.flatten([
      styles.badge,
      size && {
        height: size,
        width: size,
        borderRadius: size,
      },
      style,
    ]);

    return (
      <Container
        flex={false}
        middle
        center
        color={color}
        style={badgeStyles}
        {...props}>
        {children}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  badge: {
    height: theme.sizes.base,
    width: theme.sizes.base,
    borderRadius: theme.sizes.border,
  },
});
