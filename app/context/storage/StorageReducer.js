// @flow
import { STORAGE_GET_USER, STORAGE_LOADING } from '../types';

type Action = {
  type: string,
  payload: ?Object,
};
export default (state: any, action: Action) => {
  switch (action.type) {
    case STORAGE_LOADING:
      return { ...state, loading: !state.loading };
    case STORAGE_GET_USER:
      return { ...state, user: action.payload };
    default:
      return state;
  }
};
