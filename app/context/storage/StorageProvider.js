// flow

import React, { useReducer } from 'react';
import { config } from '../../constants';
import AsyncStorage from '@react-native-community/async-storage';
import storageReducer from './StorageReducer';
import { STORAGE_GET_USER, STORAGE_LOADING } from '../types';

const StorageContext = React.createContext({
  user: null,
  loading: false,
});

function StorageProvider({ children }) {
  const initialStorage = {
    user: null,
    loading: false,
  };

  const [storage, dispatch] = useReducer(storageReducer, initialStorage);

  const getUser = () => {
    dispatch({ type: STORAGE_LOADING });
    AsyncStorage.getItem(config.USER_STORAGE_KEY).then(data => {
      if (data) dispatch({ type: STORAGE_GET_USER, payload: data });
      dispatch({ type: STORAGE_LOADING });
    });
  };
  getUser();
  return (
    <StorageContext.Provider value={[storage, getUser]}>
      {children}
    </StorageContext.Provider>
  );
}

export { StorageProvider, StorageContext };
