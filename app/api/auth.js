// @flow
import { config } from '../constants';
import * as http from './http';

export function login(creds: {}) {
  return http.post(`${config.API_URL}/login`, creds);
}
