// @flow

import axios from 'axios';

const baseHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export function getAuthToken() {
  return null;
}

export function post(url: string, data: any, headers: any) {
  const requestHeaders = Object.assign({}, baseHeaders, headers);
  return axios.post(url, data, { headers: requestHeaders });
}

export function put(url: string, data: any, headers: any) {
  const requestHeaders = Object.assign({}, baseHeaders, headers);
  return axios.put(url, data, { headers: requestHeaders });
}

export function patch(url: string, data: any, headers: any) {
  const requestHeaders = Object.assign({}, baseHeaders, headers);
  return axios.patch(url, data, { headers: requestHeaders });
}

export function httpDelete(url: string, data: any, headers: any) {
  const requestHeaders = Object.assign({}, baseHeaders, headers);
  return axios.delete(url, { headers: requestHeaders }, data);
}

export function get(url: string, headers: Object = {}) {
  const requestHeaders = Object.assign({}, baseHeaders, headers);
  return axios.get(url, { headers: requestHeaders });
}
