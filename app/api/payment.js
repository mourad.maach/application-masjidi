// @flow

import { config } from '../constants';
import AsyncStorage from '@react-native-community/async-storage';
import * as api from './http';

export async function add(payment: Object, queryString: string) {
  const storage = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (storage) {
    const data = JSON.parse(storage);
    const { token_acces } = data;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return api.post(
      `${config.API_URL}/donation?${queryString}`,
      payment,
      headers,
    );
  }
}

export async function adherentPayments(adherentId: number) {
  const storage = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (storage) {
    const data = JSON.parse(storage);
    const { token_acces } = data;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return api.get(
      `${config.API_URL}/donations/adherent/${adherentId}`,
      headers,
    );
  }
}

export async function deletePayment(donatinId: number) {
  const storage = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (storage) {
    const data = JSON.parse(storage);
    const { token_acces } = data;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return api.httpDelete(
      `${config.API_URL}/donation/${donatinId}`,
      {},
      headers,
    );
  }
}
