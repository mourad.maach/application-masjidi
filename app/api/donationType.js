// @flow

import { config } from '../constants';
import AsyncStorage from '@react-native-community/async-storage';
import * as api from './http';

export async function addDonationType(name: string) {
  const value = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (value) {
    const data = JSON.parse(value);
    const { token_acces } = data;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return api.post(`${config.API_URL}/donation-type`, { name }, headers);
  }
}

export async function editDonationType(id: number, name: string) {
  const value = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (value) {
    const data = JSON.parse(value);
    const { token_acces } = data;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return api.put(`${config.API_URL}/donation-type/${id}`, { name }, headers);
  }
}

export async function fetchDonationType(id: number) {
  const value = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (value) {
    const data = JSON.parse(value);
    const { token_acces } = data;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return api.get(`${config.API_URL}/donation-type/${id}`, headers);
  }
}

export async function fetchAll() {
  const value = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (value) {
    const data = JSON.parse(value);
    const { token_acces } = data;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return api.get(`${config.API_URL}/donation-types`, headers);
  }
}
