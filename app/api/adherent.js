// @flow

import { config } from '../constants';
import * as http from './http';
import AsyncStorage from '@react-native-community/async-storage';

export function addAdherent(data: any, mosques: []) {
  return http.post(
    `${config.API_URL}/adherent?mosques=${mosques.toString()}`,
    data,
  );
}

export async function getAdherent(id: number) {
  const storage = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (storage) {
    const data = JSON.parse(storage);
    const { token_acces } = data;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return http.get(`${config.API_URL}/adherent/${id}`, headers);
  }
}

export async function getAdherentMosques() {
  const storage = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (storage) {
    const data = JSON.parse(storage);
    const { token_acces, applicationUser } = data;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return http.get(
      `${config.API_URL}/adherent/${applicationUser.id}/mosques`,
      headers,
    );
  }
}
