import * as adherentApi from './adherent';
import * as mosqueApi from './mosque';
import * as authApi from './auth';
import * as donationTypeApi from './donationType';
import * as paymentApi from './payment';

export { mosqueApi, adherentApi, authApi, donationTypeApi, paymentApi };
