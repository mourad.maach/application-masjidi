// @flow

import * as api from './http';

import AsyncStorage from '@react-native-community/async-storage';
import { config } from '../constants';

export function createMosque(data: any) {
  return api.post(`${config.API_URL}/mosque`, data);
}

export function getMosques() {
  return api.get(`${config.API_URL}/mosques`);
}

export async function getMosque() {
  const storage = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (storage) {
    const data = JSON.parse(storage);
    const { token_acces, applicationUser } = data;
    const { mosqueName } = applicationUser;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return api.get(
      `${config.API_URL}/mosque/${mosqueName.toLowerCase()}`,
      headers,
    );
  }
}

export async function getMosqueAdherents() {
  const storage = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
  if (storage) {
    const data = JSON.parse(storage);
    const { token_acces, applicationUser } = data;
    const { mosqueName } = applicationUser;
    const headers = { Authorization: `Bearer ${token_acces}` };
    return api.get(
      `${config.API_URL}/mosque/${mosqueName.toLowerCase()}/adherents`,
      headers,
    );
  }
}
