//  @flow

const capitalize = (str: string) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};
const paymentFr = (payment: string) => {
  switch (payment) {
    case 'CHEQUE':
      return 'Chéque';
    case 'CASH':
      return 'Espèce';
    case 'TRANSFER':
      return 'Versement';
  }
};
export { capitalize, paymentFr };
