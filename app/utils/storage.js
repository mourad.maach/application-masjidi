// @flow
import AsyncStorage from '@react-native-community/async-storage';
import { Alert } from 'react-native';

const storeData = async (key: string, data: any) => {
  try {
    return await AsyncStorage.setItem(key, JSON.stringify(data));
  } catch (e) {
    Alert.alert(`can't write data to storage - ${e}`);
  }
};

const removeData = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    Alert.alert(`can't delete data from storage - ${e}`);
  }
};

const getData = async (key: string) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      // value previously stored
      return value;
    }
  } catch (e) {
    Alert.alert(`can't delete data from storage - ${e}`);
  }
};

export { storeData, removeData, getData };
