import fr from './traduction_fr';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';

i18next.use(initReactI18next).init({
  lng: 'fr',
  resources: {
    fr: fr,
  },
  fallbackLng: 'fr',
  debug: true,
  interpolation: { escapeValue: false },
});
export default i18next;
