const colors = {
  accent: '#F3534A',
  primary: '#06BEBD',
  secondary: '#696D6F',
  tertiaryLight: '#FDE8BB',
  black: '#323643',
  white: '#FFFFFF',
  gray: '#9DA3B4',
  gray2: '#C5CCD6',
  lightGray: '#B3BDC2',
  lightGray_1: '#E8E8E8',
  lightGray_2: '#B3BDC2',
  whiteSmoke: '#F5F5F5',
  lightGray_3: '#F8F8F8',
  inputBg: '#F7F7F8',
};

const sizes = {
  // global sizes
  base: 16,
  font: 14,
  radius: 6,
  padding: 25,

  // font sizes
  h1: 26,
  h2: 20,
  h3: 18,
  title: 18,
  header: 16,
  body: 14,
  caption: 12,
};

const fonts = {
  h1: {
    fontSize: sizes.h1,
  },
  h2: {
    fontSize: sizes.h2,
  },
  h3: {
    fontSize: sizes.h3,
  },
  header: {
    fontSize: sizes.header,
  },
  title: {
    fontSize: sizes.title,
  },
  body: {
    fontSize: sizes.body,
  },
  caption: {
    fontSize: sizes.caption,
  },
};

export { colors, sizes, fonts };
