import { Alert, ScrollView, StyleSheet } from 'react-native';
import { Button, Container, Divider, Input, Switch, Text } from '../components';
// @flow
import React, { Component } from 'react';
import { adherentApi, mosqueApi } from '../api';

import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import SwitchSelector from 'react-native-switch-selector';
import Toast from 'react-native-simple-toast';
import i18n from '../i18n/i18n';
import { theme } from '../constants';

type State = {
  mosqueName: string,
  associationAddress: string,
  associationPhone: string,
  associationBankId: string,
  firstname: string,
  lastname: string,
  email: string,
  password: string,
  passwordConfirm: string,
  termsOfService: boolean,
  gender: boolean,
  asso: [],
  listAsso: Array<any>,
};

class Signup extends Component<*, State> {
  state = {
    mosqueName: '',
    firstname: '',
    lastname: '',
    email: '',
    associationBankId: '',
    associationAddress: '',
    associationPhone: '',
    password: '',
    passwordConfirm: '',
    termsOfService: false,
    gender: true,
    listAsso: [],
    asso: [],
  };

  onAssoChange = (selectedAsso: any) => {
    this.setState({ asso: selectedAsso });
  };

  componentDidMount() {
    mosqueApi
      .getMosques()
      .then(response => {
        if (response.status === 202) {
          this.setState({ listAsso: response.data });
          console.log(response.data);
        }
      })
      .catch(error => Alert.alert(error));
  }

  onSubmitAsso() {
    // check if we got valid data before submit
    if (!this.canSubmit()) return false;
    // get data from the state
    const assoData = Object.assign({}, this.state);
    const { navigation } = this.props;
    delete assoData.passwordConfirm;
    delete assoData.termsOfService;
    delete assoData.gender;
    mosqueApi
      .createMosque(assoData)
      .then(response => {
        if (response.status === 201) {
          const { data } = response;
          Alert.alert(`Association ${data.mosqueName} crée avec succès`);
          navigation.navigate('Login');
        } else {
          Toast.show("Ta demande n'a pas pu être traitée", Toast.LONG);
        }
      })
      .catch(error => {
        Toast.show(
          `Votre demande n'a pas pu être traitée - ${error}`,
          Toast.LONG,
        );
      });
  }

  onSubmitAdherent() {
    // check data before form submit
    if (!this.canSubmit()) return false;
    // build the adherent object and call and the api
    const adherent = Object.assign({}, this.state);
    const { asso } = this.state;
    const { navigation } = this.props;
    // remove unused fields
    delete adherent.listMosques;
    delete adherent.mosques;
    delete adherent.passwordConfirm;
    delete adherent.termsOfService;
    adherentApi
      .addAdherent(adherent, asso)
      .then(response => {
        if (response.status === 201) {
          const { firstname, lastname } = response.data;
          Alert.alert(
            `${firstname.toUpperCase()} ${lastname.toUpperCase()}, i18n.t('signup.creation_success')`,
          );
          navigation.navigate('Login');
        } else {
          Alert.alert(i18n.t('signup.creation_fail'));
        }
      })
      .catch(error => Alert.alert(i18n.t('signup.creation_fail')));
  }

  // validate data before submit
  canSubmit() {
    const {
      mosqueName,
      firstname,
      lastname,
      email,
      password,
      passwordConfirm,
      termsOfService,
    } = this.state;
    if (!mosqueName || !firstname || !lastname || !email || !password) {
      Alert.alert('Merci de remplir tous Les champs obligatoires');
      return false;
    } else if (password !== passwordConfirm) {
      Alert.alert('Les mots de passe ne correspondent pas');
      return false;
    } else if (!termsOfService) {
      Alert.alert("Merci d'accepter les conditions générales d'utilisations");
      return false;
    }
    return true;
  }

  render() {
    const { navigation } = this.props;
    const asso = [
      // this is the parent or 'item'
      {
        name: i18n.t('signup.list_asso'),
        id: 0,
        // these are the children or 'sub items'
        children: this.state.listAsso.map(asso_child => {
          return { id: asso_child.id, name: asso_child.mosqueName };
        }),
      },
    ];
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.root}>
        <Container style={styles.mainWrapper}>
          <SwitchSelector
            initial={0}
            onPress={value => this.setState({ gender: value })}
            buttonColor={theme.colors.primary}
            hasPadding
            options={[
              { label: i18n.t('signup.member'), value: true },
              { label: i18n.t('signup.organization'), value: false },
            ]}
          />
          <Divider margin={[theme.sizes.base / 2]} />
          <Container middle>
            {/* Informations personnelles */}
            <Container style={styles.infoPersoContainer}>
              <Text caption color="primary" style={styles.sectionTitle}>
                {i18n.t('signup.info_perso')}
              </Text>
              {!this.state.gender && (
                <Input
                  placeholder={i18n.t('signup.name_asso')}
                  style={styles.input}
                  defaultValue={this.state.mosqueName}
                  onChangeText={value =>
                    this.setState({ mosqueName: value.trim() })
                  }
                />
              )}
              <Input
                placeholder={i18n.t('signup.name')}
                style={styles.input}
                defaultValue={this.state.lastname}
                onChangeText={value =>
                  this.setState({ lastname: value.trim() })
                }
              />
              <Input
                placeholder={i18n.t('signup.firstname')}
                style={styles.input}
                defaultValue={this.state.firstname}
                onChangeText={value =>
                  this.setState({ firstname: value.trim() })
                }
              />
              {this.state.gender && (
                <SectionedMultiSelect
                  items={asso}
                  uniqueKey="id"
                  subKey="children"
                  selectText={i18n.t('signup.add_asso')}
                  confirmText={i18n.t('signup.select_asso')}
                  searchPlaceholderText={i18n.t('signup.search_asso')}
                  showDropDowns={true}
                  readOnlyHeadings={true}
                  onSelectedItemsChange={this.onAssoChange}
                  selectedItems={this.state.asso}
                  colors={{ primary: theme.colors.primary }}
                  styles={multiSelectStyles}
                />
              )}
            </Container>
            {/* Informations du compte */}
            <Container style={styles.accountInfoContainer}>
              <Text caption color="primary" style={styles.sectionTitle}>
                {i18n.t('signup.account_info')}
              </Text>
              <Input
                email
                placeholder={i18n.t('signup.email')}
                style={styles.input}
                defaultValue={this.state.email}
                onChangeText={value => this.setState({ email: value.trim() })}
              />
              <Input
                secure
                placeholder={i18n.t('signup.passwd')}
                style={styles.input}
                defaultValue={this.state.password}
                onChangeText={value =>
                  this.setState({ password: value.trim() })
                }
              />
              <Input
                secure
                placeholder={i18n.t('signup.confirm_passwd')}
                style={styles.input}
                defaultValue={this.state.passwordConfirm}
                onChangeText={value =>
                  this.setState({ passwordConfirm: value.trim() })
                }
              />
            </Container>
          </Container>
          <Container middle>
            <Container row center space="between">
              <Button onPress={() => navigation.navigate('TermsOfService')}>
                <Text
                  color={this.state.termsOfService ? 'primary' : 'gray2'}
                  style={{ textDecorationLine: 'underline' }}>
                  {i18n.t('signup.agree_terms')}
                </Text>
              </Button>
              <Switch
                value={this.state.termsOfService}
                onValueChange={value =>
                  this.setState({ termsOfService: value })
                }
              />
            </Container>
            <Button
              color="primary"
              onPress={
                this.state.gender
                  ? () => this.onSubmitAdherent()
                  : () => this.onSubmitAsso()
              }
              style={styles.button}>
              <Text body bold white center>
                {i18n.t('signup.account_create')}
              </Text>
            </Button>
          </Container>
        </Container>
      </ScrollView>
    );
  }
}

const multiSelectStyles = StyleSheet.create({
  container: {
    borderColor: theme.colors.gray2,
    borderWidth: StyleSheet.hairlineWidth,
  },
  selectToggle: {
    marginTop: theme.sizes.base,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.gray2,
    height: theme.sizes.base * 2,
    borderRadius: theme.sizes.radius,
    paddingTop: theme.sizes.base / 2,
  },
  selectToggleText: {
    color: theme.colors.gray2,
  },
});
// the screen style
const styles = StyleSheet.create({
  root: {
    padding: theme.sizes.base,
  },
  mainWrapper: {
    marginBottom: theme.sizes.base / 2,
  },
  marginBottom: {
    marginBottom: theme.sizes.base,
  },
  input: {
    width: '100%',
    marginBottom: -theme.sizes.base,
  },
  button: {
    marginTop: theme.sizes.base,
    marginBottom: theme.sizes.base,
  },
  infoPersoContainer: {
    borderWidth: 1,
    borderColor: theme.colors.lightGray,
    padding: theme.sizes.base / 2,
    marginBottom: theme.sizes.base * 2,
    borderRadius: theme.sizes.radius / 2,
  },
  sectionTitle: {
    marginTop: -(theme.sizes.base + 2),
    backgroundColor: theme.colors.white,
    width: theme.sizes.base * 10,
  },
  associationInfoContainer: {
    borderWidth: 1,
    borderColor: theme.colors.lightGray,
    padding: theme.sizes.base / 2,
    marginBottom: theme.sizes.base * 2,
    borderRadius: theme.sizes.radius / 2,
  },
  accountInfoContainer: {
    borderWidth: 1,
    borderColor: theme.colors.lightGray,
    padding: theme.sizes.base / 2,
    marginBottom: theme.sizes.base * 2,
    borderRadius: theme.sizes.radius / 2,
  },
});

export default Signup;
