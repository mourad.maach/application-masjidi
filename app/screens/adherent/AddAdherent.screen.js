// @flow

import { Alert, Dimensions, ScrollView, StyleSheet } from 'react-native';
import {
  Button,
  Container,
  Divider,
  Input,
  Switch,
  Text,
} from '../../components';
import React, { Component } from 'react';
import { adherentApi, mosqueApi } from '../../api';

import Icon from 'react-native-vector-icons/AntDesign';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import i18n from '../../i18n/i18n.js';
import { theme } from '../../constants';

type State = {
  firstname: string,
  lastname: string,
  phoneNumber: string,
  mosques: [],
  email: string,
  password: string,
  passwordConfirm: string,
  termsOfService: boolean,
  listMosques: Array<any>,
};

type Props = { navigation: Object };

const { width } = Dimensions.get('window');

class AddAdherent extends Component<Props, State> {
  state = {
    listMosques: [],
    firstname: '',
    lastname: '',
    phoneNumber: '',
    mosques: [],
    email: '',
    password: '',
    passwordConfirm: '',
    termsOfService: false,
  };

  onMosquesChange = (selectedMosques: any) => {
    // selectedFruits is array of { label, value }
    this.setState({ mosques: selectedMosques });
  };

  async componentDidMount() {
    const result = await mosqueApi.getMosques();
    result
      .catch(error => Alert.alert(JSON.stringify(error)))
      .then(response => {
        console.log(response);
        this.setState({ listMosques: response.data });
        if (response.status === 202) {
          this.setState({ listMosques: response.data });
        }
      });
  }

  onSubmitAdherent() {
    // check data before form submit
    if (!this.canSubmit()) return false;
    // build the adherent object and call and the api
    const adherent = Object.assign({}, this.state);
    const { mosques } = this.state;
    const { navigation } = this.props;
    // remove unused fields
    delete adherent.listMosques;
    delete adherent.mosques;
    delete adherent.passwordConfirm;
    delete adherent.termsOfService;
    adherentApi
      .addAdherent(adherent, mosques)
      .then(response => {
        if (response.status === 201) {
          const { firstname, lastname } = response.data;
          Alert.alert(
            `${firstname.toUpperCase()} ${lastname.toUpperCase()}, i18n.t('accountAdh.creation_success')`,
          );
          navigation.navigate('Login');
        } else {
          Alert.alert(i18n.t('accountAdh.creation_fail'));
        }
      })
      .catch(error => Alert.alert(i18n.t('accountAdh.creation_fail')));
  }

  // validate data before submit
  canSubmit() {
    const {
      firstname,
      lastname,
      email,
      password,
      mosques,
      passwordConfirm,
      termsOfService,
    } = this.state;
    if (!firstname || !lastname || !email || !password) {
      Alert.alert(i18n.t('accountAdh.error_champ'));
      return false;
    } else if (!mosques.length) {
      Alert.alert(i18n.t('accountAdh.error_asso'));
      return false;
    } else if (password !== passwordConfirm) {
      Alert.alert(i18n.t('accountAdh.error_mdp'));
      return false;
    } else if (!termsOfService) {
      Alert.alert(i18n.t('accountAdh.error_cond_util'));
      return false;
    }
    return true;
  }

  render() {
    const { navigation } = this.props;
    const mosques = [
      // this is the parent or 'item'
      {
        name: i18n.t('accountAdh.list_asso'),
        id: 0,
        // these are the children or 'sub items'
        children: this.state.listMosques.map(mosque => {
          return { id: mosque.id, name: mosque.mosqueName };
        }),
      },
    ];
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.root}>
        <Container style={styles.wrapper}>
          <Container middle>
            <Text h3 color="primary" center bold>
              <Icon name="adduser" size={theme.sizes.base * 2} />
              {i18n.t('accountAdh.title')}
            </Text>
          </Container>
          <Divider margin={[theme.sizes.base]} />
          <Container middle>
            <Input
              placeholder={i18n.t('accountAdh.name')}
              style={styles.input}
              defaultValue={this.state.lastname}
              onChangeText={value => this.setState({ lastname: value.trim() })}
            />
            <Input
              placeholder={i18n.t('accountAdh.firstname')}
              style={styles.input}
              defaultValue={this.state.firstname}
              onChangeText={value => this.setState({ firstname: value.trim() })}
            />
            <Input
              email
              placeholder={i18n.t('accountAdh.email')}
              style={styles.input}
              defaultValue={this.state.email}
              onChangeText={value => this.setState({ email: value.trim() })}
            />
            <Input
              placeholder={i18n.t('accountAdh.phone')}
              style={styles.input}
              defaultValue={this.state.phoneNumber}
              onChangeText={value =>
                this.setState({ phoneNumber: value.trim() })
              }
            />
          </Container>
          <SectionedMultiSelect
            items={mosques}
            uniqueKey="id"
            subKey="children"
            selectText={i18n.t('accountAdh.add_asso')}
            confirmText={i18n.t('accountAdh.select_asso')}
            searchPlaceholderText={i18n.t('accountAdh.search_asso')}
            showDropDowns={true}
            readOnlyHeadings={true}
            onSelectedItemsChange={this.onMosquesChange}
            selectedItems={this.state.mosques}
            colors={{ primary: theme.colors.primary }}
            styles={multiSelectStyles}
          />
          <Container middle>
            <Input
              secure
              placeholder={i18n.t('accountAdh.passwd')}
              style={styles.input}
              defaultValue={this.state.password}
              onChangeText={value => this.setState({ password: value.trim() })}
            />
            <Input
              secure
              placeholder={i18n.t('accountAdh.confirm_passwd')}
              style={styles.input}
              defaultValue={this.state.passwordConfirm}
              onChangeText={value =>
                this.setState({ passwordConfirm: value.trim() })
              }
            />

            <Container row center space="between">
              <Button onPress={() => navigation.navigate('TermsOfService')}>
                <Text
                  color={this.state.termsOfService ? 'primary' : 'gray2'}
                  style={{ textDecorationLine: 'underline' }}>
                  {i18n.t('accountAdh.agree_terms')}
                </Text>
              </Button>
              <Switch
                value={this.state.termsOfService}
                onValueChange={value =>
                  this.setState({ termsOfService: value })
                }
              />
            </Container>

            <Button
              color="primary"
              onPress={() => this.onSubmitAdherent()}
              style={styles.button}>
              <Text body bold white center>
                {i18n.t('accountAdh.create')}
              </Text>
            </Button>
          </Container>
        </Container>
      </ScrollView>
    );
  }
}
// multi-select custom style
const multiSelectStyles = StyleSheet.create({
  container: {
    borderColor: theme.colors.gray2,
    borderWidth: StyleSheet.hairlineWidth,
  },
  selectToggle: {
    marginTop: theme.sizes.base * 2,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.gray2,
    height: theme.sizes.base * 3,
    borderRadius: theme.sizes.radius,
    paddingTop: theme.sizes.base / 2,
  },
  selectToggleText: {
    color: theme.colors.gray2,
  },
});
// the screen style
const styles = StyleSheet.create({
  root: {
    padding: theme.sizes.base,
  },
  wrapper: {
    marginBottom: theme.sizes.base,
  },
  marginBottom: {
    marginBottom: theme.sizes.base,
  },
  input: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.gray2,
    width: width - theme.sizes.base * 2,
    marginBottom: -theme.sizes.base,
  },
  button: {
    marginTop: theme.sizes.base,
    marginBottom: theme.sizes.base,
  },
});

export default AddAdherent;
