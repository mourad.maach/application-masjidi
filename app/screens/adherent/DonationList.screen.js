import React, { useEffect, useState } from 'react';
import { SafeAreaView, FlatList, StyleSheet, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import Icon from 'react-native-vector-icons/FontAwesome';

import { theme, config } from '../../constants';
import { Container, Text, LoadingIndicator, Button } from '../../components';
import { paymentApi } from '../../api';
import { capitalize, paymentFr } from '../../utils/string';

function DonationList(props) {
  const { navigation } = props;
  const [role, setRole] = useState('');
  const [adherent, setAdherent] = useState(null);
  const [donations, setDonations] = useState([]);

  const getStorage = async () => {
    const storage = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
    if (storage) {
      const data = JSON.parse(storage);
      const { roles } = data;
      const currentRole = roles[0];
      setRole(currentRole);
      if (currentRole === config.ROLE_MOSQUE) {
        setAdherent(navigation.getParam('adherent', null));
      } else {
        setAdherent(data.applicationUser);
      }
    }
  };

  const getAdherentDonations = () => {
    paymentApi
      .adherentPayments(adherent.id)
      .catch(e => Toast.show(e))
      .then(response => {
        if (response.status === 200) {
          const { content } = response.data;
          setDonations(content);
        }
      });
  };

  useEffect(() => {
    getStorage();
  }, []);

  useEffect(() => {
    if (adherent) getAdherentDonations();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [adherent]);

  const deleteDonation = (id: number) => {
    setDonations([]);
    paymentApi
      .deletePayment(id)
      .then(response => {
        if (response.status && response.data === true) getAdherentDonations();
      })
      .catch(e => alert(e));
  };
  // Donation list items
  const DonationItem = ({ donation }) => {
    const { id, donationType, donationDate, mosque, amount } = donation;
    return (
      <Container block row style={styles.listItemWrapper}>
        <Container block>
          <Text h3 color="primary" bold>
            {capitalize(donationType.name)}
          </Text>
          <Text small color="gray2">
            {donationDate}
          </Text>
          <Text small bold color="gray2">
            {mosque.mosqueName}
          </Text>
        </Container>
        <Container block center>
          <Text h3 color="primary" center>
            {`${amount} €`}
          </Text>
          <Text body bold center color="gray2">
            {paymentFr(donation.paymentType)}
          </Text>
        </Container>
        <Container block middle row right>
          <Button style={styles.button} onPress={() => deleteDonation(id)}>
            <Text color={theme.colors.gray2}>
              <Icon name="trash-o" size={theme.sizes.base * 2} />
            </Text>
          </Button>
        </Container>
      </Container>
    );
  };

  return donations && adherent ? (
    <Container padding={[0, theme.sizes.base]}>
      <Text h3 color={theme.colors.gray2}>
        {`Liste des donations de ${capitalize(adherent.firstname)} ${capitalize(
          adherent.lastname,
        )}`}
      </Text>
      <Container block top margin={[theme.sizes.base / 2, 0]}>
        <SafeAreaView>
          <FlatList
            data={donations}
            renderItem={({ item }) => <DonationItem donation={item} />}
            keyExtractor={(item, index) => index.toString()}
          />
        </SafeAreaView>
      </Container>
      <Container style={styles.buttonsContainer} flex={0.15} row block middle>
        <Button color="gray2" onPress={() => false}>
          <Text center>
            <Icon name="file-pdf-o" size={theme.sizes.base * 1.5} />
          </Text>
          <Text caption>Imprimer</Text>
        </Button>
        <Button
          color="primary"
          onPress={() =>
            navigation.navigate(
              role === config.ROLE_MOSQUE
                ? 'AddMosquePayment'
                : 'AddAdherentPayment',
              { adherentId: adherent.id },
            )
          }>
          <Text center>
            <Icon name="money" size={theme.sizes.base * 1.5} />
          </Text>
          <Text caption>Nouveau don</Text>
        </Button>
      </Container>
    </Container>
  ) : (
    <LoadingIndicator />
  );
}

const styles = StyleSheet.create({
  marginTop: {
    marginTop: theme.sizes.base,
  },

  listItemWrapper: {
    backgroundColor: theme.colors.lightGray_3,
    borderRadius: theme.sizes.radius,
    justifyContent: 'space-around',
    marginVertical: theme.sizes.base / 6,
    padding: theme.sizes.base / 2,
  },

  button: {
    width: theme.sizes.base * 2,
    height: theme.sizes.base * 2,
    backgroundColor: 'transparent',
    padding: 0,
  },
  buttonsContainer: {
    justifyContent: 'space-between',
  },
});

export default DonationList;
