import React, { useState, useEffect } from 'react';
import Toast from 'react-native-simple-toast';
import {
  ScrollView,
  StyleSheet,
  Picker,
  Dimensions,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import IconFA5 from 'react-native-vector-icons/FontAwesome5';
import DateTimePicker from '@react-native-community/datetimepicker';
import {
  Container,
  Text,
  Divider,
  Input,
  Button,
  LoadingIndicator,
} from '../../components';
import { theme, config } from '../../constants';
import { simpleFormat } from '../../utils/date';
import { donationTypeApi, mosqueApi, paymentApi } from '../../api';

const { width } = Dimensions.get('window');
const PAYMENT_TYPE = [
  { label: 'Chéque', value: 'CHEQUE' },
  { label: 'Espèce', value: 'CASH' },
  { label: 'Versement', value: 'TRANSFER' },
];

function AddPayment(props) {
  const { navigation } = props;
  const currentDate = new Date(Date.now());
  const [date, setDate] = useState(currentDate);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [donationType, setDonationType] = useState([]);
  const [mosque, setMosque] = useState(null);
  const [adherents, setAdherents] = useState([]);
  const [amount, setAmount] = useState(0);
  const [adherentId, setAdherentId] = useState(
    navigation.getParam('adherentId', 0),
  );
  const [donationTypeId, setDonationTypeId] = useState(0);
  const [paymentType, setPaymentType] = useState();

  const fetchDonationType = () => {
    donationTypeApi
      .fetchAll()
      .then(response => {
        if (response.status === 200) setDonationType(response.data.content);
      })
      .catch(e =>
        Alert.alert(`Impossible de charger les données du serveur: ${e}`),
      );
  };

  const fetchMoqueAdherents = () => {
    mosqueApi
      .getMosqueAdherents()
      .then(response => {
        if (response.status === 200) {
          setAdherents(response.data);
        }
      })
      .catch(e => Alert.alert(e));
  };

  const getMosque = async () => {
    const storage = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
    if (storage) {
      const data = JSON.parse(storage);
      setMosque(data.applicationUser);
    }
  };

  useEffect(() => {
    getMosque();
  }, []);

  useEffect(() => {
    fetchMoqueAdherents();
  }, []);

  useEffect(() => {
    fetchDonationType();
  }, []);

  const onDatePicked = (event: any, value: Date) => {
    setShowDatePicker(false);
    if (value) setDate(value);
  };

  const onSubmitPayment = () => {
    if (!amount) {
      Toast.show('Merci de préciser le montant du don', Toast.LONG);
      return false;
    }
    if (!donationTypeId || !paymentType || !adherentId) {
      Toast.show('Tous les champs sont obligatoires', Toast.LONG);
      return false;
    }
    const payment = {
      amount: amount,
      donationDate: simpleFormat(date),
      paymentType: paymentType,
    };
    const params = {
      adherent: adherentId,
      mosque: mosque.id,
      donation_type: donationTypeId,
    };
    const queryString = Object.keys(params)
      .map(key => `${key}=${params[key]}`)
      .join('&');
    paymentApi
      .add(payment, queryString)
      .catch(e => Toast.show(`Erreur ${e.getMessage()}`))
      .then(response => {
        if (response.status === 200 && response.data) {
          navigation.navigate('Dashboard');
          Toast.show('Don enregistré avec succès', Toast.LONG);
        }
      });
  };

  return adherents ? (
    <ScrollView showsVerticalScrollIndicator={false} style={styles.root}>
      <Container style={styles.wrapper}>
        <Container middle>
          <Text h3 color="primary" transform="uppercase" center bold>
            <IconFA5 name="donate" size={theme.sizes.base * 2} /> Nouveau
            vesrsement
          </Text>
        </Container>
        <Divider margin={[theme.sizes.base]} />
        <Container middle>
          {showDatePicker && (
            <DateTimePicker
              value={date}
              mode="date"
              display="default"
              onChange={onDatePicked}
            />
          )}
          <Container row>
            <Input
              editable={false}
              placeholder="Date du versement"
              style={styles.dateInput}
              value={simpleFormat(date)}
            />
            <Button
              onPress={() => setShowDatePicker(true)}
              style={styles.datePickerBtn}>
              <IconFA5
                name="calendar-alt"
                size={theme.sizes.base * 2}
                color={theme.colors.primary}
              />
            </Button>
          </Container>
          <Container block>
            <Container block style={styles.pickerWrapper}>
              <Picker
                selectedValue={donationTypeId || ''}
                style={[styles.picker]}
                onValueChange={itemValue => setDonationTypeId(itemValue)}>
                <Picker.Item label="Type de don" key={0} />
                {donationType.map(dt => (
                  <Picker.Item label={dt.name} value={dt.id} key={dt.id} />
                ))}
              </Picker>
            </Container>
            <Container block style={styles.pickerWrapper}>
              <Picker
                selectedValue={paymentType || ''}
                style={[styles.picker]}
                onValueChange={itemValue => setPaymentType(itemValue)}>
                <Picker.Item label="Type de paiement" key={0} />
                {PAYMENT_TYPE.map((item, index) => (
                  <Picker.Item
                    label={item.label}
                    value={item.value}
                    key={index + 1}
                  />
                ))}
              </Picker>
            </Container>
            <Container block style={styles.pickerWrapper}>
              <Picker
                selectedValue={adherentId || ''}
                style={[styles.picker]}
                onValueChange={itemValue => setAdherentId(itemValue)}>
                <Picker.Item label="Sélectionner l'adherent" key={0} />
                {adherents.map(ad => (
                  <Picker.Item
                    label={`${ad.firstname.toUpperCase()} ${ad.lastname.toUpperCase()}`}
                    value={ad.id}
                    key={ad.id}
                  />
                ))}
              </Picker>
            </Container>
            <Input
              number
              placeholder="Montant(€)"
              style={styles.input}
              onChangeText={setAmount}
            />
          </Container>
        </Container>
        <Container block>
          <Button
            color="primary"
            onPress={onSubmitPayment}
            style={styles.button}>
            <Text body bold white center>
              Enregistrer
            </Text>
          </Button>
        </Container>
      </Container>
    </ScrollView>
  ) : (
    <LoadingIndicator />
  );
}

const styles = StyleSheet.create({
  root: {
    padding: theme.sizes.base * 2,
  },
  wrapper: {
    marginBottom: theme.sizes.base,
  },
  datePickerBtn: {
    marginLeft: theme.sizes.padding,
    paddingTop: theme.sizes.base / 2,
  },
  dateInput: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.gray2,
    width: width / 2,
    marginBottom: -theme.sizes.base / 2,
    textAlign: 'center',
  },
  input: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.gray2,
    width: width - theme.sizes.base * 4,
    marginBottom: -theme.sizes.base / 2,
  },
  picker: {
    height: theme.sizes.base * 3,
    width: width - theme.sizes.base * 4,
  },
  pickerWrapper: {
    marginTop: theme.sizes.base,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.gray2,
    borderRadius: theme.sizes.radius,
  },
});

export default AddPayment;
