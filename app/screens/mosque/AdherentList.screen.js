// @flow

import React, { useState, useEffect } from 'react';
import { StyleSheet, ScrollView, SafeAreaView, FlatList } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/EvilIcons';
import Toast from 'react-native-simple-toast';

import { Container, LoadingIndicator, Text, Button } from '../../components';
import { mosqueApi } from '../../api';
import { theme, config } from '../../constants';
import { capitalize } from '../../utils/string';

type Props = {
  navigation: {
    navigate: (routeName: string, parmas: ?Object) => void,
  },
};

function AdherentList(props: Props) {
  const { navigation } = props;
  const [adherents, setAdherents] = useState([]);
  const [mosque, setMosque] = useState(null);

  // get mosque from storage
  const getMosque = async () => {
    try {
      const storage = await AsyncStorage.getItem(config.USER_STORAGE_KEY);
      if (storage) {
        const data = JSON.parse(storage);
        setMosque(data.applicationUser);
      }
    } catch (e) {
      Toast.show(`async storage error, ${e.message}`, Toast.LONG);
    }
  };

  useEffect(() => {
    getMosque();
  }, []);

  useEffect(() => {
    mosqueApi
      .getMosqueAdherents()
      .then(response => {
        if (response.status === 200) {
          const { data } = response;
          setAdherents(data);
        }
      })
      .catch(e => Toast.show(JSON.stringify(e), Toast.LONG));
    return () => setAdherents([]);
  }, []);

  const AdherentItem = ({ adherent }) => {
    return (
      <Container block row style={styles.listItemWrapper}>
        <Container middle flex={0.5} center>
          <Icon
            name="user"
            size={theme.sizes.base * 4}
            color={theme.colors.gray2}
          />
        </Container>
        <Container block top>
          <Text h4 bold color="primary">
            {capitalize(adherent.firstname)} {capitalize(adherent.lastname)}
          </Text>
          <Text body bold color="gray2">
            N° {adherent.id}
          </Text>
          <Text body bold>
            {adherent.phoneNumber}
          </Text>
        </Container>
        <Container block row right>
          <Button style={styles.button}>
            <Icon
              name="pencil"
              size={theme.sizes.base * 2.5}
              color={theme.colors.gray}
            />
          </Button>
          <Button
            style={styles.button}
            onPress={() =>
              navigation.navigate('DonationList', {
                adherent: adherent,
              })
            }>
            <Icon
              name="credit-card"
              size={theme.sizes.base * 2.5}
              color={theme.colors.primary}
            />
          </Button>
          <Button style={styles.button}>
            <Icon
              name="minus"
              size={theme.sizes.base * 2.5}
              color={theme.colors.accent}
            />
          </Button>
        </Container>
      </Container>
    );
  };
  return adherents && mosque ? (
    <ScrollView showsVerticalScrollIndicator={false} style={styles.root}>
      <Container block style={styles.container}>
        <Container middle style={styles.title}>
          <Text h3 color={theme.colors.gray2}>
            {`Mosquée de ${capitalize(mosque.mosqueName)}`}
          </Text>
        </Container>
        <Container top>
          <SafeAreaView>
            <FlatList
              data={adherents}
              renderItem={({ item }) => {
                return <AdherentItem adherent={item} />;
              }}
              keyExtractor={(item, index) => index.toString()}
            />
          </SafeAreaView>
        </Container>
      </Container>
    </ScrollView>
  ) : (
    <LoadingIndicator />
  );
}

const styles = StyleSheet.create({
  container: {
    padding: theme.sizes.base,
  },
  listItemWrapper: {
    backgroundColor: theme.colors.lightGray_3,
    borderRadius: theme.sizes.radius,
    marginVertical: theme.sizes.base / 6,
    padding: theme.sizes.base / 2,
  },

  row: { flexDirection: 'row' },
  rowText: {
    padding: theme.sizes.base / 2,
    textAlign: 'center',
  },
  title: {
    marginBottom: theme.sizes.padding / 2,
  },

  button: {
    width: theme.sizes.base * 2.5,
    height: theme.sizes.base * 2.5,
    backgroundColor: 'transparent',
    padding: 0,
  },
});
export default AdherentList;
