// @flow

import { Alert, Dimensions, ScrollView, StyleSheet } from 'react-native';
import {
  Button,
  Container,
  Divider,
  Input,
  Switch,
  Text,
} from '../../components';
import React, { Component } from 'react';

import Icon from 'react-native-vector-icons/FontAwesome5';
import Toast from 'react-native-simple-toast';
import { mosqueApi } from '../../api';
import { theme } from '../../constants';

type State = {
  mosqueName: string,
  associationName: string,
  associationAddress: string,
  associationPhone: string,
  associationBankAccount: string,
  firstname: string,
  lastname: string,
  email: string,
  positionHeld: string,
  password: string,
  passwordConfirm: string,
  termsOfService: boolean,
};

class AddMosque extends Component<*, State> {
  state = {
    mosqueName: '',
    associationName: '',
    associationAddress: '',
    associationPhone: '',
    associationBankAccount: '',
    firstname: '',
    lastname: '',
    email: '',
    positionHeld: '',
    password: '',
    passwordConfirm: '',
    termsOfService: false,
  };

  submitMosque() {
    // check if we got valid data before submit
    if (!this.canSubmit()) return false;
    // get data from the state
    const mosqueData = Object.assign({}, this.state);
    const { navigation } = this.props;
    delete mosqueData.passwordConfirm;
    delete mosqueData.termsOfService;
    mosqueApi
      .createMosque(mosqueData)
      .then(response => {
        if (response.status === 201) {
          const { data } = response;
          Alert.alert(`mosquée ${data.mosqueName} créée avec succès`);
          navigation.navigate('Login');
        } else {
          Toast.show("Votre demande n'a pas pu être traitée", Toast.LONG);
        }
      })
      .catch(error => {
        Toast.show(
          `Votre demande n'a pas pu être traitée - ${error}`,
          Toast.LONG,
        );
      });
  }

  // validate data before submit
  canSubmit() {
    const {
      mosqueName,
      associationName,
      associationAddress,
      associationPhone,
      firstname,
      lastname,
      email,
      password,
      passwordConfirm,
      termsOfService,
    } = this.state;
    if (
      !mosqueName ||
      !associationName ||
      !associationAddress ||
      !associationPhone ||
      !firstname ||
      !lastname ||
      !email ||
      !password
    ) {
      Alert.alert('Merci de remplir tous Les champs obligatoires');
      return false;
    } else if (password !== passwordConfirm) {
      Alert.alert('Les mots de passe ne correspondent pas');
      return false;
    } else if (!termsOfService) {
      Alert.alert("Merci d'accepter les conditions générale d'utilisation");
      return false;
    }
    return true;
  }

  render() {
    const { navigation } = this.props;
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.root}>
        <Container style={styles.mainWrapper}>
          <Container middle>
            <Text h3 color="primary" transform="uppercase" center bold>
              <Icon name="mosque" size={theme.sizes.base * 2} /> Je représente
              une mosquée
            </Text>
          </Container>
          <Divider margin={[theme.sizes.base]} />
          <Container middle>
            {/* mosque informations section */}
            <Container style={styles.mosqueInfoContainer}>
              <Text caption color="primary" style={styles.sectionTitle}>
                Informations de la mosquée
              </Text>
              <Input
                placeholder="Nom de la mosquée"
                style={styles.input}
                defaultValue={this.state.mosqueName}
                onChangeText={value =>
                  this.setState({ mosqueName: value.trim() })
                }
              />
            </Container>
            {/* association informations section */}
            <Container style={styles.associationInfoContainer}>
              <Text caption color="primary" style={styles.sectionTitle}>
                Informations de l'association
              </Text>
              <Input
                placeholder="Nom de l'association"
                style={styles.input}
                defaultValue={this.state.associationName}
                onChangeText={value =>
                  this.setState({ associationName: value.trim() })
                }
              />
              <Input
                placeholder="Prénom du représentant"
                style={styles.input}
                defaultValue={this.state.firstname}
                onChangeText={value =>
                  this.setState({ firstname: value.trim() })
                }
              />
              <Input
                placeholder="Nom du représentant"
                style={styles.input}
                defaultValue={this.state.lastname}
                onChangeText={value =>
                  this.setState({ lastname: value.trim() })
                }
              />
              <Input
                placeholder="Adresse"
                style={styles.input}
                defaultValue={this.state.associationAddress}
                onChangeText={value =>
                  this.setState({ associationAddress: value.trim() })
                }
              />
              <Input
                placeholder="Numéro de téléphone"
                style={styles.input}
                defaultValue={this.state.associationPhone}
                onChangeText={value =>
                  this.setState({ associationPhone: value.trim() })
                }
              />
              <Input
                placeholder="Numéro IBAN/RIB (optionnel)"
                style={styles.input}
                defaultValue={this.state.associationBankAccount}
                onChangeText={value =>
                  this.setState({ associationBankAccount: value.trim() })
                }
              />
              <Input
                placeholder="Fonction dans l'association (optionnel)"
                style={styles.input}
                defaultValue={this.state.positionHeld}
                onChangeText={value =>
                  this.setState({ positionHeld: value.trim() })
                }
              />
            </Container>
            {/* user account information section */}
            <Container style={styles.accountInfoContainer}>
              <Text caption color="primary" style={styles.sectionTitle}>
                Informations du compte
              </Text>
              <Input
                email
                placeholder="Email"
                style={styles.input}
                defaultValue={this.state.email}
                onChangeText={value => this.setState({ email: value.trim() })}
              />
              <Input
                secure
                placeholder="Mot de passe"
                style={styles.input}
                defaultValue={this.state.password}
                onChangeText={value =>
                  this.setState({ password: value.trim() })
                }
              />
              <Input
                secure
                placeholder="confirmer le mot de passe"
                style={styles.input}
                defaultValue={this.state.passwordConfirm}
                onChangeText={value =>
                  this.setState({ passwordConfirm: value.trim() })
                }
              />
            </Container>
          </Container>

          <Container middle>
            <Container row center space="between">
              <Button onPress={() => navigation.navigate('TermsOfService')}>
                <Text
                  color={this.state.termsOfService ? 'primary' : 'gray2'}
                  style={{ textDecorationLine: 'underline' }}>
                  Condition générale d'utilisation
                </Text>
              </Button>
              <Switch
                value={this.state.termsOfService}
                onValueChange={value =>
                  this.setState({ termsOfService: value })
                }
              />
            </Container>

            <Button
              color="primary"
              onPress={() => this.submitMosque()}
              style={styles.button}>
              <Text body bold white center>
                Créer mon compte
              </Text>
            </Button>
          </Container>
        </Container>
      </ScrollView>
    );
  }
}

// the screen style
const styles = StyleSheet.create({
  root: {
    padding: theme.sizes.base,
  },
  mainWrapper: {
    marginBottom: theme.sizes.base,
  },
  marginBottom: {
    marginBottom: theme.sizes.base,
  },
  input: {
    width: '100%',
    marginBottom: -theme.sizes.base,
  },
  button: {
    marginTop: theme.sizes.base,
    marginBottom: theme.sizes.base,
  },
  mosqueInfoContainer: {
    borderWidth: 1,
    borderColor: theme.colors.lightGray,
    padding: theme.sizes.base / 2,
    marginBottom: theme.sizes.base * 2,
    borderRadius: theme.sizes.radius / 2,
  },
  sectionTitle: {
    marginTop: -(theme.sizes.base + 2),
    backgroundColor: theme.colors.white,
    width: theme.sizes.base * 10,
  },
  associationInfoContainer: {
    borderWidth: 1,
    borderColor: theme.colors.lightGray,
    padding: theme.sizes.base / 2,
    marginBottom: theme.sizes.base * 2,
    borderRadius: theme.sizes.radius / 2,
  },
  accountInfoContainer: {
    borderWidth: 1,
    borderColor: theme.colors.lightGray,
    padding: theme.sizes.base / 2,
    marginBottom: theme.sizes.base * 2,
    borderRadius: theme.sizes.radius / 2,
  },
});

export default AddMosque;
