// @flow

import { Alert, Dimensions, Image, StyleSheet, View } from 'react-native';
import { Button, Container, LoadingIndicator, Text } from '../components';
import { NavigationActions, StackActions } from 'react-navigation';
import React, { useEffect, useState } from 'react';
import { config, theme } from '../constants';

import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/Entypo';
import Video from 'react-native-video';
import { get } from '../api/http';
import i18n from '../i18n/i18n';

const { width, height } = Dimensions.get('window');

function Welcome(props: any) {
  const { navigation } = props;
  const [isApiUp, setIsApiUp] = useState(true);
  const [loading, setLoading] = useState(true);

  /*useEffect(() => {
    get(`${config.API_URL}/api/check/masjidi`)
      .catch(e => {
        Alert.alert(JSON.stringify(e));
        setIsApiUp(false);
      })
      .then(response => {
        Alert.alert(JSON.stringify(response));
        if (response.status === 202 && response.data === 'OK') {
          setIsApiUp(true);
        }
      });
  }, []);*/

  useEffect(() => {
    if (isApiUp) {
      AsyncStorage.getItem(config.USER_STORAGE_KEY).then(data => {
        if (data) {
          const redirect = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
          });
          navigation.dispatch(redirect);
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isApiUp]);

  useEffect(() => {
    const timer = setTimeout(() => setLoading(false), 1000 * 2);
    return () => {
      clearTimeout(timer);
    };
  }, []);

  return loading ? (
    <LoadingIndicator />
  ) : (
    <Container style={styles.root}>
      {/* welcome text */}
      <Container center top flex={0.2}>
        {isApiUp ? null : (
          <Text
            h3
            center
            bold
            color={theme.colors.accent}
            style={styles.notice}>
            <Icon name="emoji-sad" size={theme.sizes.base * 1.5} />
            {i18n.t('welcome.bdd_ko')}
          </Text>
        )}
      </Container>
      {/* welcome image */}

      <View style={styles.container}>
        <Video
          source={require('../assets/video/masjidi4.mp4')} // Can be a URL or a localfile.
          ref={ref => {
            this.player = ref;
          }} // Store reference
          onBuffer={this.onBuffer} // Callback when remote video is buffering
          onEnd={this.onEnd} // Callback when playback finishes
          onError={this.videoError} // Callback when video cannot be loaded
          style={styles.backgroundVideo}
          repeat={false}
          resizeMode={'cover'}
          rate={1.0}
        />
      </View>

      {/* bottom buttons */}
      <Container middle flex={0.8} margin={[0, theme.sizes.padding]}>
        <Button
          color={isApiUp ? 'primary' : 'gray2'}
          onPress={() => navigation.navigate('Login')}
          disabled={!isApiUp}>
          <Text center bold white>
            {i18n.t('welcome.login')}
          </Text>
        </Button>
        <Button
          disabled={!isApiUp}
          shadow
          onPress={() => navigation.navigate('Signup')}>
          <Text center bold>
            {i18n.t('welcome.signup')}
          </Text>
        </Button>
        <Button
          disabled={!isApiUp}
          color={isApiUp ? 'primary' : 'gray2'}
          onPress={() => navigation.navigate('AddAdherent')}>
          <Text bold center white>
            {i18n.t('welcome.login_adh')}
          </Text>
        </Button>
        <Button
          disabled={!isApiUp}
          color={isApiUp ? 'primary' : 'gray2'}
          onPress={() => navigation.navigate('AddMosque')}>
          <Text bold center white>
            {i18n.t('welcome.login_asso')}
          </Text>
        </Button>
      </Container>
    </Container>
  );
}

const styles = StyleSheet.create({
  root: {
    marginTop: theme.sizes.base,
  },
  welcomeImage: {
    width: width / 1.2,
    height: height / 2,
    overflow: 'visible',
  },
  container: { flex: 0.4, justifyContent: 'center' },
  notice: {
    borderColor: theme.colors.accent,
    paddingLeft: theme.sizes.base,
    paddingRight: theme.sizes.base,
    paddingTop: theme.sizes.base / 2,
    paddingBottom: theme.sizes.base / 2,
    borderRadius: theme.sizes.radius,
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    alignItems: 'stretch',
    bottom: 0,
    right: 0,
  },
});

Welcome.navigationOptions = { header: null };

export default Welcome;
