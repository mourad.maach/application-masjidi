// @flow

import { Button, Container, Input, Text } from '../components';
import { KeyboardAvoidingView, StyleSheet } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import React, { useEffect, useState } from 'react';
import { config, theme } from '../constants';
import { removeData, storeData } from '../utils/storage';

import Toast from 'react-native-simple-toast';
import { authApi } from '../api';
import i18n from '../i18n/i18n';

function Login(props: any) {
  const { navigation } = props;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [validation, setValidation] = useState({
    email: {
      error: false,
      message: '',
    },
    password: {
      error: false,
      message: '',
    },
  });
  // make sure to remove the user from the storage
  useEffect(() => {
    removeData(config.USER_STORAGE_KEY);
  }, []);

  const onLoginSubmit = () => {
    if (!email) {
      setValidation({
        ...validation,
        email: { error: true, message: i18n.t('login.error_email') },
      });
      return false;
    }

    if (!password) {
      setValidation({
        ...validation,
        password: { error: true, message: i18n.t('login.error_mdp') },
      });
      return false;
    }
    // call the login action
    authApi
      .login({ email, password })
      .then(response => {
        const user = response.data;
        storeData(config.USER_STORAGE_KEY, user);
        const redirect = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
        });
        navigation.dispatch(redirect);
      })
      .catch(error => {
        Toast.show(i18n.t('login.error_login'), Toast.LONG);
      });
  };

  return (
    <KeyboardAvoidingView style={styles.root} behavior="height">
      <Container padding={[0, theme.sizes.base]}>
        {/* login jumbarton */}
        <Text h1 bold center color="secondary">
          {i18n.t('login.title')}
        </Text>
        <Container middle>
          <Input
            error={validation.email.error}
            label={validation.email.message}
            email
            placeholder={i18n.t('login.email')}
            style={styles.input}
            defaultValue={email}
            onChangeText={value => setEmail(value.trim())}
          />
          <Input
            error={validation.password.error}
            label={validation.password.message}
            secure
            placeholder={i18n.t('login.passwd')}
            style={styles.input}
            defaultValue={password}
            onChangeText={value => setPassword(value.trim())}
          />
          <Button color="primary" onPress={onLoginSubmit}>
            <Text body bold white center>
              {i18n.t('login.identification')}
            </Text>
          </Button>

          <Button onPress={() => navigation.navigate('PasswordForgotten')}>
            <Text center gray>
              {i18n.t('login.forget_passwd')}
            </Text>
          </Button>
        </Container>
      </Container>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
  },

  border: {
    borderColor: theme.colors.grey,
    borderWidth: 1,
  },
});
export default Login;
