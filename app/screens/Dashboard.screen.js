// @flow
import React, { useState, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import { theme, config } from '../constants';
import { removeData } from '../utils/storage';
import Icon from 'react-native-vector-icons/FontAwesome';

import { Container, Text, Button, LoadingIndicator } from '../components';

import AsyncStorage from '@react-native-community/async-storage';

type MenuItem = { name: string, icon: string, screen: string };

export const menuAdherent = [
  {
    name: 'Mon Compte',
    icon: 'user',
    screen: 'Profile',
  },
  {
    name: 'Versement',
    icon: 'credit-card',
    screen: 'DonationList',
  },
  {
    name: 'Poser une question',
    icon: 'question-circle',
    screen: 'AskQuestion',
  },
  {
    name: 'Proposer une idée',
    icon: 'lightbulb-o',
    screen: 'SuggestIdea',
  },
  {
    name: 'Faire un don',
    icon: 'money',
    screen: 'MakeDonation',
  },
  {
    name: 'Informations',
    icon: 'bell-o',
    screen: 'Information',
  },
  {
    name: 'Contact',
    icon: 'envelope-o',
    screen: 'Contact',
  },
];

export const menuMosque = [
  {
    name: 'Mon Compte',
    icon: 'user',
    screen: 'Profile',
  },
  {
    name: 'Adhérent',
    icon: 'users',
    screen: 'AdherentList',
  },
  {
    name: 'Versement',
    icon: 'credit-card',
    screen: 'AddMosquePayment',
  },
  {
    name: 'Poser une question',
    icon: 'question-circle',
    screen: 'AskQuestion',
  },
  {
    name: 'Proposer une idée',
    icon: 'lightbulb-o',
    screen: 'SuggestIdea',
  },
  {
    name: 'PUSH Mail',
    icon: 'at',
    screen: 'PushMail',
  },
  {
    name: 'Informations',
    icon: 'bell-o',
    screen: 'Information',
  },
  {
    name: 'Contact',
    icon: 'envelope-o',
    screen: 'Contact',
  },
];

type Props = { navigation: * };

function Dashboard({ navigation }: Props) {
  const [user, setUser] = useState(null);
  const [role, setRole] = useState(null);
  useEffect(() => {
    AsyncStorage.getItem(config.USER_STORAGE_KEY).then(value => {
      const data = JSON.parse(value);
      setUser(data.applicationUser);
      setRole(data.roles[0]);
    });
  }, []);

  const onLogout = () => {
    removeData(config.USER_STORAGE_KEY);
    const redirect = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Welcome' })],
    });
    navigation.dispatch(redirect);
  };
  // get the menu items based on the logged in role
  const menuItems = role === config.ROLE_MOSQUE ? menuMosque : menuAdherent;

  return user ? (
    <ScrollView showsVerticalScrollIndicator={false} style={styles.root}>
      <Container block>
        <Container top>
          <Text h1 color="primary" transform="uppercase" center>
            {role === config.ROLE_MOSQUE
              ? `Mosquée de ${user.mosqueName}`
              : `Bienvenue ${user.firstname} ${user.lastname}`}
          </Text>
        </Container>
        <Container style={styles.menuGrid} middle>
          {menuItems.map((item: MenuItem) => (
            <TouchableOpacity
              onPress={() => navigation.navigate(item.screen)}
              style={styles.menuItem}
              key={Math.random() * 10}>
              <Icon
                name={item.icon}
                size={theme.sizes.base * 3}
                color={theme.colors.primary}
              />
              <Text h3 center color="primary">
                {item.name}
              </Text>
            </TouchableOpacity>
          ))}
        </Container>
        <Container>
          <Button color="primary" onPress={onLogout}>
            <Text body bold center white transform="uppercase">
              <Icon name="long-arrow-left" size={theme.sizes.base} /> {` `}
              Se déconnecter
            </Text>
          </Button>
        </Container>
      </Container>
    </ScrollView>
  ) : (
    <LoadingIndicator />
  );
}

const styles = StyleSheet.create({
  root: {
    display: 'flex',
    margin: theme.sizes.base,
  },
  menuGrid: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: theme.sizes.base,
  },
  menuItem: {
    alignItems: 'center',
    width: '50%',
    padding: theme.sizes.base / 1.2,
  },
});

export default Dashboard;
