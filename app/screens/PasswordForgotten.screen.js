// @flow

import { Button, Container, Input, Text } from '../components';
import { KeyboardAvoidingView, StyleSheet } from 'react-native';
import React, { Component } from 'react';

import Icon from 'react-native-vector-icons/EvilIcons';
import i18n from '../i18n/i18n';
import { theme } from '../constants';

type State = { email: string };
class PasswordForgetten extends Component<*, State> {
  state = {
    email: '',
  };
  render() {
    return (
      <KeyboardAvoidingView style={styles.root} behavior="height">
        <Container padding={[0, theme.sizes.base]} middle>
          <Text h2 center color="primary">
            <Icon name="lock" size={theme.sizes.base * 10} />
          </Text>
          <Text h2 color="secondary" center>
            {i18n.t('forget.title')}
          </Text>
          <Text header center color="gray" style={styles.marginTop}>
            {i18n.t('forget.description')}
          </Text>
          {/* login form */}
          <Container middle>
            <Input
              label={i18n.t('forget.email')}
              style={styles.input}
              defaultValue={this.state.email}
              onChangeText={value => this.setState({ email: value })}
            />
            <Button color="primary" onPress={() => {}}>
              <Text body bold white center>
                {i18n.t('forget.renvoi_mdp')}
              </Text>
            </Button>
          </Container>
        </Container>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
  },

  border: {
    borderColor: theme.colors.grey,
    borderWidth: 1,
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  marginTop: {
    marginTop: theme.sizes.base,
  },
});
export default PasswordForgetten;
